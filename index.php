<!DOCTYPE html>
<html>
<head>
	<title>2048 Game</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="2048 Game with jQuery">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="board">
		<?php for ($i=1; $i <= 4 ; $i++) { 
			?>
			<div class="board_row">
				<?php
				for ($j=1; $j <= 4 ; $j++) { 
					?><div class="board_cell" id="cell_<?php echo $i; ?>_<?php echo $j; ?>"></div><?php
				}
				?>
			</div>
			<?php
		}?>
		<div class="game_result_container">
			<div class="game_result"></div>
			<a href="javascript:;" id="restart_game">Restart Game</a>
		</div>
	</div>
	<div class="score_container">Score: <span class="score"></span></div>
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/functions.js"></script>
</body>
</html>