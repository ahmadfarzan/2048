var score;
var game;
var grid = [];
grid['up'] = [];
grid['up'][0] = [];
grid['up'][1] = [];
grid['up'][2] = [];
grid['up'][3] = [];
grid['down'] = [];
grid['down'][0] = [];
grid['down'][1] = [];
grid['down'][2] = [];
grid['down'][3] = [];
grid['left'] = [];
grid['left'][0] = [];
grid['left'][1] = [];
grid['left'][2] = [];
grid['left'][3] = [];
grid['right'] = [];
grid['right'][0] = [];
grid['right'][1] = [];
grid['right'][2] = [];
grid['right'][3] = [];
document.onkeydown = checkKey;

$(document).ready(function(){
	init();
	$("#restart_game").click(function() {
		init();
	});
});
function initGrid() {
	grid['up'][0][0] = $('#cell_4_1');
	grid['up'][0][1] = $('#cell_4_2');
	grid['up'][0][2] = $('#cell_4_3');
	grid['up'][0][3] = $('#cell_4_4');

	grid['up'][1][0] = $('#cell_3_1');
	grid['up'][1][1] = $('#cell_3_2');
	grid['up'][1][2] = $('#cell_3_3');
	grid['up'][1][3] = $('#cell_3_4');

	grid['up'][2][0] = $('#cell_2_1');
	grid['up'][2][1] = $('#cell_2_2');
	grid['up'][2][2] = $('#cell_2_3');
	grid['up'][2][3] = $('#cell_2_4');

	grid['up'][3][0] = $('#cell_1_1');
	grid['up'][3][1] = $('#cell_1_2');
	grid['up'][3][2] = $('#cell_1_3');
	grid['up'][3][3] = $('#cell_1_4');


	grid['down'][0][0] = $('#cell_1_1');
	grid['down'][0][1] = $('#cell_1_2');
	grid['down'][0][2] = $('#cell_1_3');
	grid['down'][0][3] = $('#cell_1_4');

	grid['down'][1][0] = $('#cell_2_1');
	grid['down'][1][1] = $('#cell_2_2');
	grid['down'][1][2] = $('#cell_2_3');
	grid['down'][1][3] = $('#cell_2_4');

	grid['down'][2][0] = $('#cell_3_1');
	grid['down'][2][1] = $('#cell_3_2');
	grid['down'][2][2] = $('#cell_3_3');
	grid['down'][2][3] = $('#cell_3_4');

	grid['down'][3][0] = $('#cell_4_1');
	grid['down'][3][1] = $('#cell_4_2');
	grid['down'][3][2] = $('#cell_4_3');
	grid['down'][3][3] = $('#cell_4_4');


	grid['left'][0][0] = $('#cell_4_4');
	grid['left'][0][1] = $('#cell_3_4');
	grid['left'][0][2] = $('#cell_2_4');
	grid['left'][0][3] = $('#cell_1_4');

	grid['left'][1][0] = $('#cell_4_3');
	grid['left'][1][1] = $('#cell_3_3');
	grid['left'][1][2] = $('#cell_2_3');
	grid['left'][1][3] = $('#cell_1_3');

	grid['left'][2][0] = $('#cell_4_2');
	grid['left'][2][1] = $('#cell_3_2');
	grid['left'][2][2] = $('#cell_2_2');
	grid['left'][2][3] = $('#cell_1_2');

	grid['left'][3][0] = $('#cell_4_1');
	grid['left'][3][1] = $('#cell_3_1');
	grid['left'][3][2] = $('#cell_2_1');
	grid['left'][3][3] = $('#cell_1_1');


	grid['right'][0][0] = $('#cell_4_1');
	grid['right'][0][1] = $('#cell_3_1');
	grid['right'][0][2] = $('#cell_2_1');
	grid['right'][0][3] = $('#cell_1_1');

	grid['right'][1][0] = $('#cell_4_2');
	grid['right'][1][1] = $('#cell_3_2');
	grid['right'][1][2] = $('#cell_2_2');
	grid['right'][1][3] = $('#cell_1_2');

	grid['right'][2][0] = $('#cell_4_3');
	grid['right'][2][1] = $('#cell_3_3');
	grid['right'][2][2] = $('#cell_2_3');
	grid['right'][2][3] = $('#cell_1_3');

	grid['right'][3][0] = $('#cell_4_4');
	grid['right'][3][1] = $('#cell_3_4');
	grid['right'][3][2] = $('#cell_2_4');
	grid['right'][3][3] = $('#cell_1_4');
}

function getIGrid(location) {
	return grid[location];
}

function checkKey(e) {
	var validKey = false;
	var won = false;
	e = e || window.event;
	if(!game) {
		return true;
	}
	if (e.keyCode == '38') {
		validKey = true;
		var moved = false;
		var iGrid = getIGrid('up');
	} else if (e.keyCode == '40') {
		validKey = true;
		var iGrid = getIGrid('down');
	} else if (e.keyCode == '37') {
		validKey = true;
		var iGrid = getIGrid('left');
	} else if (e.keyCode == '39') {
		validKey = true;
		var iGrid = getIGrid('right');
	}

	if(validKey) {
		for (var i = 2; i >= 0; i--) {
			for (var j = 0; j <= 3; j++) {
				for (var k = i; k < 3; k++) {
					if(iGrid[k][j].text()==="") {

					} else if(iGrid[k+1][j].text()==="") {
						moved = true;
						if(iGrid[k][j].text()!=="") {
							var scoreFromMove = parseInt(iGrid[k][j].text())
							iGrid[k+1][j].attr('data-score', scoreFromMove).text(scoreFromMove);
						}
						iGrid[k][j].attr('data-score', '').text("");
					} else if(iGrid[k+1][j].text()===iGrid[k][j].text()) {
						moved = true;
						var scoreFromMove = parseInt(iGrid[k+1][j].text())+parseInt(iGrid[k][j].text());
						if(scoreFromMove>=2048) {
							won = true;
							game = false;
						}
						score += scoreFromMove;
						iGrid[k+1][j].attr('data-score', scoreFromMove).text(scoreFromMove);
						iGrid[k][j].attr('data-score', '').text("");
					}
				}
			}
		}
	}

	if(validKey && moved) {
		if(!fillRandomCell()) {
			game = false;
		}
	} else if(validKey && getAvailableCells().length===0) {
		game = false;
	}

	if(won && !game) {
		$(".game_result").text("You Win!");
		$(".game_result_container").show();
	} else if(!game) {
		$(".game_result").text("Game Over");
		$(".game_result_container").show();
	} else {
		$(".score").text(score);
	}
}

function init() {
	$(".board_cell").attr('data-score', '')
	$(".board_cell").html("");
	score = 0;
	game = true;
	fillRandomCell();
	fillRandomCell();
	initGrid();
	$(".score").text(score);
	$(".game_result").text("");
	$(".game_result_container").hide();
}

function getAvailableCells() {
	var emptyCells = $(".board_cell:empty");
	return emptyCells;
}

function getRandomAvailableCell() {
	var emptyCells = getAvailableCells();
	var emptyCellsCount = emptyCells.length;
	if(emptyCellsCount>0){
		var randomEmptyCellIndex = Math.floor((Math.random() * emptyCellsCount));
		return emptyCells[randomEmptyCellIndex];
	} else {
		return false;
	}
}

function fillRandomCell() {
	var randomCell = getRandomAvailableCell();
	if(randomCell!==false) {
		$(randomCell).attr('data-score', '2').fadeTo('slow', 0.5).fadeTo('slow', 1.0).text('2');
		return true;
	} else {
		return false;
	}
}